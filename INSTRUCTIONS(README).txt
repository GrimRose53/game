EXPEDITION - game title


OVERVIEW:
The year is 2024. You play as an explorer/tomb raider named Abel. Abel's partner and friend, Jackson, recently found intel on
an supposedly abandoned island rummored to still contain unknown valuables. You travel to the island expecting an easy cash 
grab, but the island wasn't as abandoned as Jackson said. fight the enemies, solve puzzles/avoid traps, and collect the 
valuables and leave the island to complete your 'mission.' (TLDR: Collect the four idols around the island, defeat enemies 
and then you're done. also find the EasterEgg hidden somewhere in the map{optional but kinda fun to find/funny when you find
it})


CONTROLS/TUTORIAL:
Familiarize yourself with the controls. You can always look back at this readme file for reminders or simply look in the 
Settings menu for the list of controls and more (such as goals, lore, enemy stat info, and 
W - move forward
A - move left
S - move backward
D - move right
Scroll Wheel (on mouse) - zoom in/out
Space - jump
Left CRTL - crouch down (also fires the gun)
Left Shift - Sprint
Left Click (on mouse) - fire projectile
ESC - brings up pause menu and closes it


GOALS:
1.) Defeat the enemies
2.) Gather the idols/collectibles
3.) After gathering all the items, return to the beach where you landed on the island in the first place
4.) Have fun (not manditory) 
5.) Explore (not manditory but would be appreciated)


LORE:
The Island - The island, located two hundred miles off the coast of japan and commonly referred to as "The Beast's Den,"
(or The Den for short) has stayed out of public eye for several centuries due to the dangerous waters surrounding the 
Den, making any ship that approaches it another addition to the expanding ship graveyard. In the current/recent year(s),
The Den has remained largely untouched and unvisited by the public. This is because the island is officially labeled as 
private property of Dennis Wellings, CEO of Wellings Tech Corp. The Beast's Den really serves as a personal getaway for
Dennis, the foundation for the ruins of a forgotten society, birth place of a twisted cult/religion, a base of 
operations for illegal tech smuggling and black market trades/sales, and as the home of a monster. 

The Cultists - Crazed lunatics that believe in a being simply known as "The Deity." They believe that their religious
ways are the true and proper ways. They have been known to capture, torture, and in the event of failing to convert
them, kill people that follow other religions. The Cultist's, also known as the "Beast Children," primarily do these
horrid capture and convert methods on people like preists and missionaries, but they have been known to take random
people that show strong wills and are devout in their respective religions. Beast Children cultists also perform cruel
torture and punishments on people who follow no religions in hopes to turn them to the side of The Deity. If the torture
is unsuccessful, the captives are killed and fed to The Beast. The Beast Children work from various places around the 
globe to achieve their agenda. Their agenda is to take over the world and show everyone the power of The Deity, to turn
the Earth into The Deity's personal playground with everyone doing its bidding.
 
The Idols - Small statues made of pure gold. Shows The Deity on one side and The Beast on the other. Made by the same
generation of Beast Children cult members that made the cave drawing. Due to pure gold's properties, the statues are
still mostly shiny and have little damage. They were clearly cared for by each generation of the cult's followers.
Also presumably worth a small fortune, each; however, that information is unclear on whether it is true or not.

The Cave Drawing - A drawing of The Deity and its counter-part The Beast left on The Beast's Den island by early members
of the Beast Children cult. Shows a portrait of both parts. The Deity which is dipicted as a being wrapped in cloth 
holding a flower. The drawing also shows The Beast, which is dipicted as a horrid monster licking its claws with a 
wicked smile across its face and its claws covered in some liquid, presumably blood.

Abel - A mercenary that focuses primarily on the job and worries little about the side effects of his actions. Takes
whatever jobs he feels are worth it. Works with his friend Jackson, who he rescued on a mission to take out a gang 
boss in Houston, Texas. Not much can be said about Abel's backstory due to the fact that he doesn't particularly 
care for telling his life story. It is known that he grew up in Chicago, Illinois, enjoys eating Philly 
cheesesteak sandwiches and philly cheesesteak HotPockets, had an abusive mother and an absent father.

Jackson - A skilled hacker and all around computer nerd who grew up in Miami, Florida. Was being forced to work for
gang members out of Houston, Texas, which allowed the gang he was working for to score several hiests and essentially 
take over the south-west part of Houston. As time went on, Jackson started secretly putting bounties on several members
of the gang. More and more gang members vanished and suspicion on Jackson raised. Jackson put one final bounty out 
before he got caught. The bounty was on the gang's boss. The hired mercenary went directly to the gang's hideout to 
fulfill the bounty. Once it was all said and done, Jackson went to thank the mercenary when a gun was pulled on him by 
the hired gun. Jackson quickly explained his situation and the merc lowered his gun. That was the day that Jackson met 
Abel. As Abel was leaving, Jackson asked if he needed any help. Abel responded with a shrug and said "do whatever you
want." Ever since that day, four years ago, Jackson has been Abel's partner, giving intel, and managing jobs for
Abel all from the safety of a secure facility in Chicago, Illinois. 

The Deity/The Beast - The truth of this being is shrouded in mystery. (Dev Note: Fancy game speak for not much is 
really known about this creature, so there isn't much to tell)

Dennis - Cult member who is blending into the modern world supplying his religion/cult with everything that they
could possibly need. His company is one of the world leaders in technology and science, both theoretical and
experimental. His public good side of "pure intentions" and philanthropy distracts the public from the illegal
black market trades and smuggling, while he also pockets the best pieces of equipment his company makes for the
cult residing on the island. (Dev Note: think of Lex Luthor, that's where I got my inspiration from. Not as 
intelligent as Lex, and Dennis' company isn't super widespread/into every field imagineable as Lex's; however, 
he is just as Dangerous)

The Rats- No real significance to any sort of plot. They are rats, that's about it. Rats, rats, they are the rats. 


ENEMIES:
Rat - has 5 health. It is a rat, a ravenous rat if you will. Shoot to kill.

Cultist - has 10 health. Crazed lunatics who will not hesitate to kill you, so kill them first.


COLLECTABLES:
Idols - four of them hidden in different parts of the island. represented by a shiny cyan capsule.


ACCREDIDATION FOR STUFF THAT I DID NOT MAKE BY MYSELF(INCLUDES TUTORIALS I FOLLOWED AND ASSETS TAKEN FROM THE STORE):

1.) First Person Controller Asset - Made by: Vitefait, License type: Extension Asset, 
License Agreement: Standard Unity Asset Store EULA

2.) Terrain Asset - Made by: Unity Technologies, License type: Extention Asset,
License Agreement: Standard Unity Asset Store EULA

3.) Start Menu Tutorial - https://www.youtube.com/watch?v=zc8ac_qUXQY
4.) Pause Menu Tutorial - https://www.youtube.com/watch?v=JivuXdrIHK0&t=13s
5.) Collectibles Tutorial - https://www.youtube.com/watch?v=hbmeCY_FUXE and https://www.youtube.com/watch?v=X7Z99NihD8g
6.) Rat Texture/Image - https://www.groupon.com/deals/gg-cm-halloween-evil-rat-2-pack 
(I took the image from google image search, this is just the website the image came from)
7.) Omelette Texture/Image - https://www.seriouseats.com/western-omelette-with-bell-pepper-onion-ham-and-cheese (same as rat, 
taken from a google image search, the link is to the website where I got it from)

8.) Spawner, Enemy Health, Player Heath, Seek Behavior, Damage Behavior, 
Game Manager, (scripts)  - Copied from the FPS project from Fundamentals of Game Programming 1.

9.) Setting up health text/number display, and crosshair - follwed from the FPS project from Fundamentals of Game Programming 1.



DISCLOSURE:
I couldn't figure out what went wrong with the buttons on my pause menu. They were working at one point but something happened
and I couldn't figure out what went wrong. Bringing up the menu will still pause the game though. So it does work just not at
100% capacity.

I also had issues with the spawners. I looked at the error messages and they said that they couln't spawn anything because the
agent wasn't close enough to the NavMesh. I looked at the scripts several times over, messed with the NavMesh and spawners to 
see if I could fix it/make them work but nothing did; I just couldn't see what was going wrong.
It honestly felt like Unity just said hey you get no spawners. The enemies function fine when I place them in the hierarchy 
without using the spawners. The spawners just decided not to work, or maybe it's a problem I am a little too dumb to notice.
So I just hand placed enemies around the scene instead.
